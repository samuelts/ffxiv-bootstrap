# FFXIV Bootstrap Theme

This is a Bootstrap theme kit made using hackerthemes.com theme kit. The color palette is pulled from in game dyes.

## How to Use

This theme works exactly like native bootstrap and includes all native bootstrap classes as well as 96 additional custom color classes matching the FFXIV in game dyes.

For example:
```
<button class="btn btn-rolanberry">rolanberry</button>
```
will render

![Rolanberry Button](./images/rolanberry.png)

These color classes can be applied to any element by appending `-colorNameHere` to your existing bootstrap classes.

For a full demo please visit [ffxiv-bootstrap](https://samuelts.com/ffxiv-bootstrap)

## Implementation

I don't currently have a CDN set up but the stylesheet can be referenced in your html `<head>` with the following:

```
<link rel="stylesheet" href="https://samuelts.com/ffxiv-bootstrap/css/ffxiv.min.css">
```

Since github is serving this url the file is gzipped and comes in at 63.7KB transfer.

Unzipped minified file size is 458KB.

Non-minified file size is 1.71MB.

## Colors

[![Color Palette](./images/palette.png)](https://samuelts.com/ffxiv-bootstrap#palette)

## Create your own Bootstrap theme

There is a full tutorial available at hackerthemes.com: [How to build your own Bootstrap themes](https://hackerthemes.com/kit).

## Creators

### This Bootstrap Theme
**Samuel Stephenson**

- <https://samuelts.com>
- <https://github.com/safwyls>
- [Vael Artyr - Gilgamesh](https://na.finalfantasyxiv.com/lodestone/character/17275058/)

### Third Parties
**Square Enix**

[吉田 直樹 Yoshida Naoki - Yoshi-P](https://en.wikipedia.org/wiki/Naoki_Yoshida)

**Alexander Rechsteiner**

- <https://hackerthemes.com>
- <https://twitter.com/arechsteiner>
- <https://github.com/arechsteiner>

**Bootstrap**

- <https://github.com/twbs/bootstrap/>

## License

Released under the [MIT License](https://opensource.org/licenses/MIT).
